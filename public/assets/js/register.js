console.log("hello from JS file");

let registerForm = document.querySelector("#registerUser");

registerForm.addEventListener("submit", (e) => {

	e.preventDefault(); // to avoid page refresh/redicrection once the event has been triggered.

	//capture each values inside the input fields.
	let firstName = document.querySelector("#firstName").value
	let lastName = document.querySelector("#lastName").value
	let userEmail = document.querySelector("#userEmail").value
	let mobileNo = document.querySelector("#mobileNumber").value
	let password = document.querySelector("#password1").value
	let verifyPassword = document.querySelector("#password2").value


	// lets create a control structure
	//->to check if passwords match
	//->to check if passwords are not empty
	if((password !== "" && verifyPassword !=="") && (verifyPassword === password) && (mobileNo.length === 11)){
		fetch('https://pure-caverns-93135.herokuapp.com/api/users/email-exists', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: userEmail
			})
		}).then(res => res.json()
		)//this will give the information if there are no duplicates found.
		.then(data => { 
			if(data === false){
			fetch('https://pure-caverns-93135.herokuapp.com/api/users/register', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					firstName: firstName,
					lastName: lastName,
					email: userEmail,
					mobileNo: mobileNo,
					password: password
				}) //this section describe the body of the request converted into a JSON format.
			}).then(res => {
				return res.json()
			}).then(data => {
				console.log(data)
				if(data === true){
					Swal.fire({
						icon: 'success',
						title: 'Sucess!',
						text: 'New account registered successfully.'
					})
				}else {
					Swal.fire({
						icon: 'error',
						title: 'Ooops!!',
						text: 'Something went wrong in the registration.'
					})
				}
			})

			}else{
				Swal.fire({
					icon: 'warning',
					title: 'Ooops!!',
					text: 'Email already exists, choose another email.'
				})

			}
		})

	} else {
		Swal.fire({
			icon: 'error',
			title: 'Ooops!!',//the value of the title will be up to the developer
			text: 'Something went wrong in the registration.' //can be used to display/show more details/information about the action/response

		})
	}

})