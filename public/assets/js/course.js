//the first thing that we need to do is identify which course it needs to display inside the browser.

//we are going to use the course id to identify the correct course properly.


let params = new URLSearchParams(window.location.search)
//window.location -> returns a location object with information about the current location of the document
//.search -> contains the query string section of the current URL. //search property returns an object of type stringString.
//URLSearchParams() -> this method/constructor creates and returns a URLSearchParams object.
//"URLSearchParams" -> describes the interface that defines utility methods to work with the query string of a URL.


//params = {
let id = params.get('courseId')
//	"courseId": ".... id ng course that we passed"
//}
console.log(id)

//lets capture the access token from the local storage
let token = localStorage.getItem('token')
//lets capture the sections of the html body
console.log(token)

let name = document.querySelector("#courseName")
let description = document.querySelector("#courseDesc")
let price = document.querySelector("#coursePrice")
let enroll = document.querySelector("#enrollmentContainer")


//display course information in jumbotron
fetch(`https://pure-caverns-93135.herokuapp.com/api/courses/${id}`).then(res => res.json()).then(data => {
	console.log(data)

	name.innerHTML = data.name
	description.innerHTML = data.description
	price.innerHTML = data.price
	enroll.innerHTML = `<a id="enrollButton" class="btn btn-success text-white btn-block">Enroll</a>`
	//we have to capture first the anchor tag for enroll, add an eventlisterner to trigger an event. Create a function in the eventListener() to describe the next set of procedures.
	document.querySelector("#enrollButton").addEventListener("click", () => {

		if(!token || token === null){
		//lets redirect the user to the login page
		//alert("You must login first")
			Swal.fire({
			  icon: 'error',
			  title: 'Oops...',
			  text: 'You must login first!',
			}).then((result) => {
				if (result.isConfirmed) window.location.href="./login.html"
			})
		}else {
			Swal.fire({
			  title: 'Are you sure you want to enroll this course?',
			  icon: 'info',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Yes!'
			}).then((result) => {
				if (result.isConfirmed) {
					fetch('https://pure-caverns-93135.herokuapp.com/api/users/enroll', {
						//describe the parts of the request.
						method: 'POST',
						headers: {
							'Content-Type': 'application/json',
			 				'Authorization': `Bearer ${token}`
						},
						body: JSON.stringify({
							courseId: id
						})
					}).then(res => res.json()
					)//this will give the information if there are no duplicates found.
					.then(data => {
						console.log(data)
						//now we can inform the user that the request has been done or not.
							if(data === true){
								Swal.fire(
							      'Success!',
							      'Course enrolled successfully',
							      'success'
							    ).then((result) => {
									if (result.isConfirmed) window.location.replace("./courses.html")
								})
							}else {
								//inform the user that the request has failed.
								Swal.fire(
							      'Oooops!',
							      'Something went wrong.',
							      'error'
							    )
							}
					})
				}
			}
		)}
	})
})


