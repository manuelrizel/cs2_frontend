
let formSubmit = document.querySelector("#createCourse");

formSubmit.addEventListener("submit", (e)=> {
	e.preventDefault();

let name = document.querySelector("#courseName").value
let cost = document.querySelector("#coursePrice").value
let desc = document.querySelector("#courseDescription").value

let formClear = () => {
	document.querySelector("#courseName").value = "";
	document.querySelector("#coursePrice").value = "";
	document.querySelector("#courseDescription").value = "";
}

	if(name !== "" && cost !== "" && desc !== ""){

		fetch('https://pure-caverns-93135.herokuapp.com/api/courses/course-exists', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					name: name.trim()
				})
			}).then(res => res.json()
			)//this will give the information if there are no duplicates found.
			.then(data => {
			if(data === false){
				fetch('https://pure-caverns-93135.herokuapp.com/api/courses/addCourse', {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						name:name,
						description: desc,
						price: cost
						
					})
				}).then(res => {
				return res.json()
				}).then(data => {
					if(data === true){
					Swal.fire({
						icon: 'success',
						title: 'Sucess!',
						text: 'New course registered successfully.'
					})
						formClear();
					}else{
					Swal.fire({
						icon: 'error',
						title: 'Ooops!!',
						text: 'Something went wrong when adding a new course.'
					})
					}
				})
			}else {
					Swal.fire({
						icon: 'warning',
						title: 'Ooops!!',
						text: 'Course name already exists, choose another name.'
					})

			}
		})
	} else {
		Swal.fire({
			icon: 'error',
			title: 'Ooops!!',
			text: 'Something went wrong when adding a new course.'
		})
	}

})