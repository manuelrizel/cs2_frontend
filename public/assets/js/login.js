console.log("hello")

let loginForm = document.querySelector('#loginUser')

loginForm.addEventListener("submit", (e) => {
	e.preventDefault();

	let email = document.querySelector('#userEmail').value
	console.log(email)
	let password= document.querySelector('#password').value
	console.log(password)

	//how can we inform the user that a blank input field cannot be logged in?

	if(email == "" || password == ""){
		Swal.fire({
			icon: 'warning',
			title: 'Ooops!!',
			text: 'Please input your email and/or password.'
		})
	}else {
		fetch('https://pure-caverns-93135.herokuapp.com/api/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		}).then(res => {
			return res.json() 
		}).then(data => {
			console.log(data.access)
			if(data.access){
				//lets save the access token inside our local storage
				localStorage.setItem('token', data.access); 
				//alert("access key saved on local storage.")
				fetch(`https://pure-caverns-93135.herokuapp.com/api/users/details`, {
					headers: { 
						'Authorization': `Bearer ${data.access}`
					}
				}).then(res => {
					return res.json();
				}).then(data => {
					console.log(data)
					//lets create a checker to see if may nakukuhang data
					localStorage.setItem("id", data._id)
					localStorage.setItem("isAdmin", data.isAdmin)
					console.log("items are set inside the local storage.")
					window.location.replace('./courses.html')
				})
			} else {
				//if there is no existing access key value from the data variable then just inform the user.
					Swal.fire({
						icon: 'error',
						title: 'Ooops!!',
						text: 'Something went wrong, check your credentials.'
					})
			}
		})
	}
})
